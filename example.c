#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

/*----------------------------------------------------------------------------*/
bool is_vowel(int c)
{
  c = toupper(c);
  if (c == 'A' || c == 'E' || c == 'I' || c == 'O' || c == 'U')
    return true;

  return false;
}

/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
  char* input = argv[1];

  // set a pointer at the beginning of the string
  char* forward_pointer = input;
  printf("input: \"%s\"\n", input);

  // set a pointer at the end of the string (assign the pointer, then increment by len)
  char* reverse_pointer = forward_pointer + strlen(input);

  while (forward_pointer < reverse_pointer)
  {
    // increment our place in the string until we hit a vowel
    while (forward_pointer < reverse_pointer && !is_vowel(*forward_pointer))
      forward_pointer++;

    // decrement our place in the string until we hit a vowel
    while (forward_pointer < reverse_pointer && !is_vowel(*reverse_pointer))
      reverse_pointer--;

    // work loop
    if (forward_pointer != reverse_pointer)
    {
      // store vowel value from forward_pointer
      char vowel = *forward_pointer;

      // write value of forward pointer with vowel captured in reverse search
      *forward_pointer++ = *reverse_pointer;
//      printf("progress 1: %s\n", input);

      // using stored vowel replace value in reverse pointer
      *reverse_pointer-- = vowel;
//      printf("progress 2: %s\n", input);
    }
  }

  // print out modified string
  printf("output: \"%s\"\n", input);
}
